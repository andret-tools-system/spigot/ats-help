/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help;

import eu.andret.ats.help.request.Request;
import eu.andret.ats.help.request.RequestManager;
import eu.andret.ats.help.request.RequestPlayer;
import eu.andret.ats.help.request.Status;
import eu.andret.ats.help.util.IdProvider;
import org.bukkit.entity.Player;
import org.mockito.testng.MockitoTestNGListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Listeners(MockitoTestNGListener.class)
public class HelpCommandTest {
	@Test
	void testInfoWithRequests() {
		// given
		final Player player = mock(Player.class);
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);
		final HelpCommand helpCommand = new HelpCommand(player, plugin, requestManager);
		requestManager.getPlayers().add(HelpPluginTestUtils.createRequestPlayer(player, 1));

		// when
		final List<String> result = helpCommand.info();

		// then
		assertThat(result).containsExactly(
				"§b#1§r: waiting [§bWAITING§r]",
				"§b#2§r: fixing [§6FIXING§r]",
				"§b#3§r: done [§aDONE§r]",
				"§b#4§r: problem [§cPROBLEM§r]");
	}

	@Test
	void testInfoWithoutRequests() {
		// given
		final Player player = mock(Player.class);
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);
		final HelpCommand helpCommand = new HelpCommand(player, plugin, requestManager);
		final RequestPlayer requestPlayer = new RequestPlayer(player);
		requestManager.getPlayers().add(requestPlayer);
		when(plugin.getMessage("noProblems")).thenReturn("Empty!");

		// when
		final List<String> result = helpCommand.info();

		// then
		assertThat(result).containsExactly("Empty!");
	}

	@Test
	void testStatusFallback() {
		// given
		final Player player = mock(Player.class);
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final HelpCommand helpCommand = new HelpCommand(player, plugin, new RequestManager(plugin));
		when(plugin.getMessage("wrongStatus")).thenReturn("%status% not in %statuses%");

		// when
		final String result = helpCommand.status("UNKNOWN");

		// then
		assertThat(result).isEqualTo("UNKNOWN not in §bWAITING§r, §6FIXING§r, §aDONE§r, §cPROBLEM§r");
	}

	@Test
	void testStatusChangeNegativeId() {
		// given
		final Player player = mock(Player.class);
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final HelpCommand helpCommand = new HelpCommand(player, plugin, new RequestManager(plugin));
		when(plugin.getMessage("noRequest")).thenReturn("Empty!");

		// when
		final String result = helpCommand.status(-11, Status.DONE);

		// then
		assertThat(result).isEqualTo("Empty!");
	}

	@Test
	void testStatusChangeWrongId() {
		// given
		final Player player = mock(Player.class);
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);
		final HelpCommand helpCommand = new HelpCommand(player, plugin, requestManager);
		when(plugin.getMessage("noRequest")).thenReturn("Empty!");

		// when
		final String result = helpCommand.status(1, Status.DONE);

		// then
		assertThat(result).isEqualTo("Empty!");
	}

	@Test
	void testStatusChange() {
		// given
		final Player player = mock(Player.class);
		final Player requesting = mock(Player.class);
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);
		final HelpCommand helpCommand = new HelpCommand(player, plugin, requestManager);
		requestManager.getPlayers().add(HelpPluginTestUtils.createRequestPlayer(requesting, 1));
		when(requesting.getPlayer()).thenReturn(requesting);
		when(plugin.getMessage("successStatus")).thenReturn("Sender: %id% -> %status%");
		when(plugin.getMessage("yoursChanged")).thenReturn("Requesting: %id% -> %status%");

		// when
		final String result = helpCommand.status(1, Status.PROBLEM);

		// then
		assertThat(result).isEqualTo("Sender: 1 -> §cPROBLEM§r");
		assertThat(requestManager.getRequestPlayer(requesting).requests().get(0).getStatus()).isSameAs(Status.PROBLEM);
		verify(requesting, times(1)).sendMessage("Requesting: 1 -> §cPROBLEM§r");
	}

	@Test
	void testEmptyList() {
		// given
		final Player player = mock(Player.class);
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);
		final HelpCommand helpCommand = new HelpCommand(player, plugin, requestManager);
		when(plugin.getMessage("header")).thenReturn("== HEADER ==");
		when(plugin.getMessage("count")).thenReturn("Count: %count%");
		when(plugin.getMessage("currentPage")).thenReturn("page %page% of %pages%");

		// when
		final List<String> result = helpCommand.list();

		// then
		assertThat(result).containsExactly("== HEADER ==", "Count: 0", "page 1 of 1");
	}

	@Test
	void testListExceededPageNumber() {
		// given
		final Player player = mock(Player.class);
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);
		final HelpCommand helpCommand = new HelpCommand(player, plugin, requestManager);
		requestManager.getPlayers().add(HelpPluginTestUtils.createRequestPlayer(player, 1));
		when(plugin.getMessage("pagesError")).thenReturn("%number% does not exist");

		// when
		final List<String> result = helpCommand.list(4);

		// then
		assertThat(result).containsExactly("4 does not exist");
	}

	@Test
	void testListWithStatus() {
		// given
		final Player player = mock(Player.class);
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);
		final HelpCommand helpCommand = new HelpCommand(player, plugin, requestManager);
		requestManager.getPlayers().add(HelpPluginTestUtils.createRequestPlayer(player, 1));
		when(player.getName()).thenReturn("ThePlayer123");
		when(plugin.getMessage("header")).thenReturn("== HEADER ==");
		when(plugin.getMessage("count")).thenReturn("Count: %count%");
		when(plugin.getMessage("currentPage")).thenReturn("page %page% of %pages%");

		// when
		final List<String> result = helpCommand.list(Status.FIXING);

		// then
		assertThat(result).containsExactly(
				"== HEADER ==",
				"Count: 1",
				"page 1 of 1",
				"ThePlayer123§b#2§r: fixing [§6FIXING§r]");
	}

	@Test
	void testNonEmptyList() {
		// given
		final Player player = mock(Player.class);
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);
		final HelpCommand helpCommand = new HelpCommand(player, plugin, requestManager);
		requestManager.getPlayers().add(HelpPluginTestUtils.createRequestPlayer(player, 1));
		when(player.getName()).thenReturn("ThePlayer123");
		when(plugin.getMessage("header")).thenReturn("== HEADER ==");
		when(plugin.getMessage("count")).thenReturn("Count: %count%");
		when(plugin.getMessage("currentPage")).thenReturn("page %page% of %pages%");

		// when
		final List<String> result = helpCommand.list();

		// then
		assertThat(result).containsExactly(
				"== HEADER ==",
				"Count: 4",
				"page 1 of 1",
				"ThePlayer123§b#1§r: waiting [§bWAITING§r]",
				"ThePlayer123§b#2§r: fixing [§6FIXING§r]",
				"ThePlayer123§b#3§r: done [§aDONE§r]",
				"ThePlayer123§b#4§r: problem [§cPROBLEM§r]");
	}

	@Test
	void testHelpEmpty() {
		// given
		final Player player = mock(Player.class);
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);
		final HelpCommand helpCommand = new HelpCommand(player, plugin, requestManager);
		when(plugin.getMessage("noText")).thenReturn("No text!");

		// when
		final String result = helpCommand.help();

		// then
		assertThat(result).isEqualTo("No text!");
		assertThat(requestManager.getPlayers()).isEmpty();
	}

	@Test
	void testHelp() {
		// given
		final Player player = mock(Player.class);
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);
		final HelpCommand helpCommand = new HelpCommand(player, plugin, requestManager);
		final IdProvider idProvider = new IdProvider();
		when(plugin.getMessage("successSend")).thenReturn("Yay, sent!");
		when(plugin.getIdProvider()).thenReturn(idProvider);

		// when
		final String result = helpCommand.help("help", "me");

		// then
		assertThat(result).isEqualTo("Yay, sent!");
		assertThat(requestManager.getPlayers()).hasSize(1);
		assertThat(requestManager.getRequestPlayer(player)).isNotNull();
		assertThat(requestManager.getRequestPlayer(player).requests()).containsExactly(new Request(1, "help me", Status.WAITING));
	}
}
