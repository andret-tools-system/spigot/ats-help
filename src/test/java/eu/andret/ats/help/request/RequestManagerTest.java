/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help.request;

import eu.andret.ats.help.HelpPlugin;
import eu.andret.ats.help.HelpPluginTestUtils;
import eu.andret.ats.help.util.IdProvider;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RequestManagerTest {
	@DataProvider(name = "getStatuses")
	private static Object[][] getStatuses() {
		return Arrays.stream(Status.values())
				.map(status -> new Object[]{status.name(), status})
				.toArray(Object[][]::new);
	}

	@Test
	public void createRequestTest() {
		// given
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);
		when(plugin.getIdProvider()).thenReturn(new IdProvider(10));

		// when
		final Request result = requestManager.createRequest("test", Status.DONE);

		// then
		assertThat(result).isEqualTo(new Request(11, "test", Status.DONE));
	}

	@Test(dataProvider = "getStatuses")
	void formatStatusTest(@NotNull final String name, @NotNull final Status status) {
		// given
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);

		// when
		final String result = requestManager.format(status);

		// then
		assertThat(result).endsWith(name + "§r");
	}

	@Test
	void formatRequestTest() {
		// given
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);

		// when
		final String result = requestManager.format(new Request(1, "test", Status.DONE));

		// then
		assertThat(result).isEqualTo("§b#1§r: test [§aDONE§r]");
	}

	@Test
	void getRequestPlayerTest() {
		// given
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final Player player = mock(Player.class);
		final RequestManager requestManager = new RequestManager(plugin);

		// when
		final RequestPlayer result = requestManager.getRequestPlayer(player);

		// then
		assertThat(result).isExactlyInstanceOf(RequestPlayer.class);
		assertThat(result.offlinePlayer()).isSameAs(player);
		assertThat(result.requests()).isEmpty();
	}

	@Test
	void getRequestPlayerTwiceTest() {
		// given
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final Player player = mock(Player.class);
		final RequestManager requestManager = new RequestManager(plugin);

		// when
		final RequestPlayer result1 = requestManager.getRequestPlayer(player);
		final RequestPlayer result2 = requestManager.getRequestPlayer(player);

		// then
		assertThat(result1).isSameAs(result2);
	}

	@Test
	void getRequestsCountTest() {
		// given
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final Player player = mock(Player.class);
		final RequestManager requestManager = new RequestManager(plugin);
		requestManager.getPlayers().add(HelpPluginTestUtils.createRequestPlayer(player, 1));
		requestManager.getPlayers().add(HelpPluginTestUtils.createRequestPlayer(player, 5));

		// when
		final long result = requestManager.getRequestsCount();

		// then
		assertThat(result).isEqualTo(8);
	}

	@Test
	void getRequestsCountByStatusTest() {
		// given
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final Player player = mock(Player.class);
		final RequestManager requestManager = new RequestManager(plugin);
		requestManager.getPlayers().add(HelpPluginTestUtils.createRequestPlayer(player, 1));
		requestManager.getPlayers().add(HelpPluginTestUtils.createRequestPlayer(player, 5));

		// when
		final long result = requestManager.getRequestsCount(Status.FIXING);

		// then
		assertThat(result).isEqualTo(2);
	}

	@Test(dataProvider = "getStatuses")
	void mapNameToStatusTest(@NotNull final String name, @NotNull final Status status) {
		// given
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);

		// when
		final Status result = requestManager.mapNameToStatus(name);

		// then
		assertThat(result).isSameAs(status);
	}

	@Test
	void mapNameToStatusUnknownTest() {
		// given
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final RequestManager requestManager = new RequestManager(plugin);

		// when
		final Status result = requestManager.mapNameToStatus("UNKNOWN");

		// then
		assertThat(result).isNull();
	}

	@Test
	void getLastUsedIdTest() {
		// given
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final Player player = mock(Player.class);
		final RequestManager requestManager = new RequestManager(plugin);
		requestManager.getPlayers().add(HelpPluginTestUtils.createRequestPlayer(player, 1));
		requestManager.getPlayers().add(HelpPluginTestUtils.createRequestPlayer(player, 5));

		// when
		final long result = requestManager.getLastUsedId();

		// then
		assertThat(result).isEqualTo(8);
	}

	@Test
	void getPlayerRequestPairTest() {
		// given
		final HelpPlugin plugin = mock(HelpPlugin.class);
		final Player player = mock(Player.class);
		final RequestManager requestManager = new RequestManager(plugin);
		final RequestPlayer requestPlayer = HelpPluginTestUtils.createRequestPlayer(player, 1);
		requestManager.getPlayers().add(requestPlayer);

		// when
		final PlayerRequestPair result = requestManager.getPlayerRequestPair(3);

		// then
		assertThat(result).isNotNull();
		assertThat(result.requestPlayer()).isSameAs(requestPlayer);
		assertThat(result.request()).isEqualTo(new Request(3, "done", Status.DONE));
	}
}
