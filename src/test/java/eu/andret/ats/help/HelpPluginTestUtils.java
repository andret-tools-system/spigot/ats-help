/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help;

import eu.andret.ats.help.request.Request;
import eu.andret.ats.help.request.RequestPlayer;
import eu.andret.ats.help.request.Status;
import org.bukkit.OfflinePlayer;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public final class HelpPluginTestUtils {
	@NotNull
	public static RequestPlayer createRequestPlayer(@NotNull final OfflinePlayer offlinePlayer, final int startingId) {
		return new RequestPlayer(offlinePlayer, List.of(
				new Request(startingId, "waiting", Status.WAITING),
				new Request(startingId + 1, "fixing", Status.FIXING),
				new Request(startingId + 2, "done", Status.DONE),
				new Request(startingId + 3, "problem", Status.PROBLEM)));
	}
}
