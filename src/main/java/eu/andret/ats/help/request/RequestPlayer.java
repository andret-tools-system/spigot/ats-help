/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help.request;

import org.bukkit.OfflinePlayer;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * The wrapper of the {@link OfflinePlayer} to store requests.
 *
 * @param offlinePlayer The wrapped player.
 * @param requests      The list of all requests the player sent.
 * @author Andret2344
 * @since Feb 12, 2021
 */
public record RequestPlayer(@NotNull OfflinePlayer offlinePlayer, @NotNull List<Request> requests) {
	public RequestPlayer(@NotNull final OfflinePlayer offlinePlayer) {
		this(offlinePlayer, new ArrayList<>());
	}
}
