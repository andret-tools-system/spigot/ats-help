/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help.request;

import org.bukkit.ChatColor;
import org.jetbrains.annotations.NotNull;

/**
 * The enum that indicates what status a particular request currently has.
 */
public enum Status {
	/**
	 * The request has been sent, maybe seen, but nobody wanted to take care of it yet.
	 */
	WAITING(ChatColor.AQUA),
	/**
	 * Someone decided to fix the issue, work in progress.
	 */
	FIXING(ChatColor.GOLD),
	/**
	 * The request is now completed.
	 */
	DONE(ChatColor.GREEN),
	/**
	 * There was a problem regarding the request.
	 */
	PROBLEM(ChatColor.RED);

	@NotNull
	private final ChatColor color;

	Status(@NotNull final ChatColor color) {
		this.color = color;
	}

	@NotNull
	public ChatColor getColor() {
		return color;
	}
}
