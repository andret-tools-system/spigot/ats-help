/*
 * Copyright Andret (c) 2025 Andret Tools System. Copying and modifying allowed only keeping git link reference.
 */

package eu.andret.ats.help;

import eu.andret.arguments.AnnotatedCommandExecutor;
import eu.andret.arguments.api.annotation.Argument;
import eu.andret.arguments.api.annotation.BaseCommand;
import eu.andret.arguments.api.annotation.Completer;
import eu.andret.arguments.api.annotation.TypeFallback;
import eu.andret.arguments.api.entity.ExecutorType;
import eu.andret.ats.help.request.PlayerRequestPair;
import eu.andret.ats.help.request.RequestManager;
import eu.andret.ats.help.request.RequestPlayer;
import eu.andret.ats.help.request.Status;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@BaseCommand("atshelp")
public class HelpCommand extends AnnotatedCommandExecutor<HelpPlugin> {
	private static final String PLACEHOLDER_STATUS = "%status%";
	private static final String PLACEHOLDER_ID = "%id%";
	private static final String PLACEHOLDER_STATUSES = "%statuses%";

	private final RequestManager requestManager;

	public HelpCommand(@NotNull final CommandSender sender, @NotNull final HelpPlugin plugin,
					   @NotNull final RequestManager requestManager) {
		super(sender, plugin);
		this.requestManager = requestManager;
	}

	@NotNull
	@Argument(executorType = ExecutorType.PLAYER, permission = "ats.help.info",
			description = "&eCheck status of your requests.")
	public List<String> info() {
		final RequestPlayer player = requestManager.getRequestPlayer((Player) sender);
		if (player.requests().isEmpty()) {
			return Collections.singletonList(plugin.getMessage("noProblems"));
		}
		return player.requests()
				.stream()
				.map(requestManager::format)
				.toList();
	}

	@NotNull
	@TypeFallback(Status.class)
	public String status(@NotNull final String status) {
		final String statuses = Stream.of(Status.values())
				.map(requestManager::format)
				.collect(Collectors.joining(", "));
		return plugin.getMessage("wrongStatus")
				.replace(PLACEHOLDER_STATUS, status)
				.replace(PLACEHOLDER_STATUSES, statuses);
	}

	@NotNull
	@Argument(executorType = ExecutorType.PLAYER, permission = "ats.help.status",
			description = "&eSet the status of certain request.")
	public String status(@Completer("anyRequestId") final int id, @NotNull final Status status) {
		if (id < 0) {
			return plugin.getMessage("noRequest");
		}
		final PlayerRequestPair result = requestManager.getPlayerRequestPair(id);
		if (result == null) {
			return plugin.getMessage("noRequest");
		}
		result.request().setStatus(status);
		Optional.of(result)
				.map(PlayerRequestPair::requestPlayer)
				.map(RequestPlayer::offlinePlayer)
				.map(OfflinePlayer::getPlayer)
				.ifPresent(player -> player.sendMessage(plugin.getMessage("yoursChanged")
						.replace(PLACEHOLDER_ID, String.valueOf(result.request().getId()))
						.replace(PLACEHOLDER_STATUS, requestManager.format(status))));
		return plugin.getMessage("successStatus")
				.replace(PLACEHOLDER_ID, String.valueOf(result.request().getId()))
				.replace(PLACEHOLDER_STATUS, requestManager.format(status));
	}

	@NotNull
	@Argument(executorType = ExecutorType.PLAYER, permission = "ats.help.list",
			description = "&eShow current requests list.")
	public List<String> list() {
		return list(1);
	}

	@NotNull
	@Argument(executorType = ExecutorType.PLAYER, permission = "ats.help.list",
			description = "&eShow current requests list of given status.")
	public List<String> list(@NotNull final Status status) {
		return list(status, 1);
	}

	@NotNull
	@Argument(executorType = ExecutorType.PLAYER, permission = "ats.help.list",
			description = "&eShow current requests list's page.")
	public List<String> list(@Completer("pagesCount") final int number) {
		final List<PlayerRequestPair> list = requestManager.getPlayers()
				.stream()
				.flatMap(requestPlayer -> requestPlayer.requests().stream()
						.map(request -> new PlayerRequestPair(requestPlayer, request)))
				.toList();
		return listRequests(number, list);
	}

	@NotNull
	@Argument(executorType = ExecutorType.PLAYER, permission = "ats.help.list",
			description = "&eShow current requests list's of given type page.")
	public List<String> list(@NotNull final Status status, @Completer("pagesStatusCount") final int number) {
		final List<PlayerRequestPair> list = requestManager.getPlayers()
				.stream()
				.flatMap(requestPlayer -> requestPlayer.requests().stream()
						.filter(request -> request.getStatus().equals(status))
						.map(request -> new PlayerRequestPair(requestPlayer, request)))
				.toList();
		return listRequests(number, list);
	}

	@NotNull
	private List<String> listRequests(final int number, @NotNull final List<PlayerRequestPair> list) {
		final int start = Math.max(5 * number - 5, 0);
		final int maxPages = list.size() / 5 + 1;
		final int currentPage = start / 5 + 1;
		if (currentPage > maxPages) {
			return Collections.singletonList(plugin.getMessage("pagesError")
					.replace("%number%", String.valueOf(currentPage)));
		}
		final List<String> message = List.of(
				plugin.getMessage("header"),
				plugin.getMessage("count")
						.replace("%count%", String.valueOf(list.size())),
				plugin.getMessage("currentPage")
						.replace("%page%", String.valueOf(currentPage))
						.replace("%pages%", String.valueOf(maxPages)));
		if (list.isEmpty()) {
			return message;
		}
		final List<String> collect = list.stream()
				.skip(start)
				.limit(5)
				.map(playerRequestPair -> playerRequestPair.requestPlayer().offlinePlayer().getName()
						+ requestManager.format(playerRequestPair.request()))
				.toList();
		final List<String> result = new ArrayList<>(collect);
		result.addAll(0, message);
		return result;
	}

	@NotNull
	@Argument(executorType = ExecutorType.PLAYER, permission = "ats.help.ask",
			description = "&eRequest the help.")
	public String help(@NotNull final String @NotNull ... values) {
		if (values.length == 0) {
			return plugin.getMessage("noText");
		}
		final RequestPlayer requestPlayer = requestManager.getRequestPlayer((Player) sender);
		requestPlayer.requests().add(requestManager.createRequest(String.join(" ", values), Status.WAITING));
		return plugin.getMessage("successSend");
	}
}
